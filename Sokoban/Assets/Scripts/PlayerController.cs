using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private Tilemap groundTilemap;
    [SerializeField]
    private Tilemap collisionTilemap;

    public Transform box;
    public Transform finish;
    private PlayerMovement controls;
    private AudioSource audio; 
    // Start is called before the first frame update
    
    private void Awake()
    {
        controls = new PlayerMovement();
       
    }
    public void Start()
    {
        controls.Main.Movement.performed += ctx => Move(ctx.ReadValue<Vector2>());
        groundTilemap = GameObject.Find("Ground").GetComponent<Tilemap>();
        collisionTilemap = GameObject.Find("Col").GetComponent<Tilemap>();
    }
    private void OnEnable()
    {
        controls.Enable();
    }
    
    private void OnDisable()
    {
        controls.Disable();
    }


    private void Move(Vector2 direction)
    {
        if (CanMove(direction) && FindObjectOfType<LevelManager>().finishState == false)
        {
           audio = GameObject.Find("MovementSound").GetComponent<AudioSource>();
            audio.Play();
            transform.position += (Vector3)direction;
            transform.position += ((Vector3)direction/3.75f);
        }
    } 
    private bool BoxPosition(Vector3Int gridPosition, Vector3Int[] boxPosition)
    {

        for (int i = 0; i < boxPosition.Length ; i++)
        {
            if (gridPosition == boxPosition[i])
            {
                box = FindObjectOfType<LevelManager>().Box[i];
                return true;
            }
        }
        return false;
    }
    private bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = groundTilemap.WorldToCell(transform.position + (Vector3)direction);
        Vector3Int boxPosition = groundTilemap.WorldToCell(box.position);
        Vector3Int[] boxPosition2 = new Vector3Int[FindObjectOfType<LevelManager>().Box.Length];

        for (int i = 0; i < FindObjectOfType<LevelManager>().Box.Length; i++)
        {
            boxPosition2[i] = groundTilemap.WorldToCell(FindObjectOfType<LevelManager>().Box[i].position);
        }
        Vector3Int finishPosition = groundTilemap.WorldToCell(finish.position);
        Vector3Int boxGridPosition = groundTilemap.WorldToCell(box.position + (Vector3)direction);
      
        if (!groundTilemap.HasTile(gridPosition) || collisionTilemap.HasTile(gridPosition))
        {
            Debug.Log("No");
            return false;
        }
        else if (BoxPosition(gridPosition, boxPosition2) == true && (!groundTilemap.HasTile(boxGridPosition) || collisionTilemap.HasTile(boxGridPosition)))
        {
            return false;
        } else if (FindObjectOfType<LevelManager>().finishState == false && BoxPosition(gridPosition, boxPosition2) == true && groundTilemap.HasTile(boxGridPosition) && !collisionTilemap.HasTile(boxGridPosition) && box.GetComponent<boxMovement>().canMove == true)
        {
            Debug.Log("Found");
            box.position += (Vector3)direction;
            box.position += ((Vector3)direction / 3.75f);
            FindObjectOfType<LevelManager>().playerMovements += 1;
            FindObjectOfType<LevelManager>().boxMovements += 1;
            return true;
        }else if (BoxPosition(gridPosition, boxPosition2) == true &&  box.GetComponent<boxMovement>().canMove == false)
        {
          
            return false;
        }else { 
        if ((FindObjectOfType<LevelManager>().finishState == false)) {
            FindObjectOfType<LevelManager>().playerMovements += 1;
        }
            return true;
        }
        
        // Check finish state of the box 

       
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
