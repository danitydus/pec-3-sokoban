using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEditor;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
public class editorManager : MonoBehaviour
{
    public Tilemap tilemap;
    public Tile[] tiles;
    public List<GameObject> UITiles;
    public List<GameObject> TilesButton;

    public List<GameObject> BoxMeta;

    public int selectedTile = 0;
    public Transform tileGridUI;

    public GameObject level;
    public SaveLevel levelEditor;

    public int actualBoxes;
    public int actualFinish;

    public GameObject advertisePanel;
    
    // Start is called before the first frame update
    void Start()
    {
        advertisePanel.SetActive(false);
        int i = 0;
        RenderUITiles();
        foreach (Tile tile in tiles)
        {
            GameObject UITile = new GameObject("UI Tile");
            UITile.transform.parent = tileGridUI;
            UITile.transform.localScale = new Vector3(1f, 1f, 1f);

            Image UIImage = UITile.AddComponent<Image>();
            UIImage.sprite = tile.sprite;

            Color tileColor = UIImage.color;
            tileColor.a = 0.5f;

            if (i == selectedTile)
            {
                tileColor.a = 1f;
            }

            UIImage.color = tileColor;

            UITiles.Add(UITile);

            i++;
        }
    }

   public void selectTile(int selectedTileId)
    {
        selectedTile = selectedTileId;
        RenderUITiles();
    }
    // Update is called once per frame
    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {
            Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (position.y > -2.7f)
            {
                tilemap.SetTile(tilemap.WorldToCell(position), tiles[selectedTile]);

                if (selectedTile == 2 )
                { 

                    actualBoxes += 1;
                    tilemap.SetTile(tilemap.WorldToCell(position), null);
                    if (actualBoxes <= 4)
                    {
                        GameObject box;
                        box = Instantiate(BoxMeta[0], position, transform.rotation);
                        box.name = "Box" + actualBoxes;
                        box.SetActive(true);
                        box.transform.SetParent(level.transform);
                        box.transform.position = new Vector3(position.x, position.y, 0);
                        FindObjectOfType<EditorLevelManager>().AddBox(box.transform, actualBoxes);
                    }
                }
                if (selectedTile == 3 && actualFinish <= 4)
                {
                    actualFinish += 1;
                    tilemap.SetTile(tilemap.WorldToCell(position), null);
                    GameObject meta;
                    meta = Instantiate(BoxMeta[1], position, transform.rotation);
                    meta.name = "Finish" + actualFinish;
                    meta.SetActive(true);
                    meta.transform.SetParent(level.transform);
                    meta.transform.position = new Vector3(position.x, position.y, 0);
                    FindObjectOfType<EditorLevelManager>().AddFinish(meta.transform, actualFinish);


                }
            }
        }
            if (Input.GetMouseButtonDown(1))
            {
                Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                tilemap.SetTile(tilemap.WorldToCell(position), null);
            }
        }
    

    public void resetEditor()
    {
        SceneManager.LoadScene(2);

    }
    public void saveLevel()
   {
        if (actualBoxes != actualFinish || actualBoxes > 4 || actualBoxes < 1)
        {
            StartCoroutine(ShowOffAdvertise());
            return;
        }
#if UNITY_EDITOR
 string localPath = "Assets/EditorLevelSO/" + "LevelCreated" + ".prefab";
            GameObject u = PrefabUtility.SaveAsPrefabAsset(level, localPath);
            levelEditor.level = u;
#endif

    }
    IEnumerator ShowOffAdvertise()
    {
        advertisePanel.SetActive(true);
        yield return new WaitForSeconds(3);
        advertisePanel.SetActive(false);
    }
    void RenderUITiles()
    {
        int i = 0;
        foreach (GameObject tile in TilesButton)
        {
            Image UIImage = tile.GetComponent<Image>();
            Color tileColor = UIImage.color;
            tileColor.a = 0.5f;
            if (i == selectedTile)
            {
                tileColor.a = 1f;
            }
            UIImage.color = tileColor;
            i++;
        }
    }
        public void ReturnMenu()
        {
            SceneManager.LoadScene(0);
        } 
    public void PlayLevel()
        {
        saveLevel();
        if (actualBoxes == actualFinish && actualBoxes <= 4 && actualBoxes > 0)
        {
            SceneManager.LoadScene(3);

        }
    }
    }

