using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class boxMovement : MonoBehaviour
{
    public bool canMove;
    public SpriteRenderer renderer;
    public Color finishColor;
    public Color initialColor;
    [SerializeField]
    private Tilemap groundTilemap;
    public Transform finish;
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        renderer = GetComponent<SpriteRenderer>();
    }
    public void FinishState()
    {
        canMove = false;
        renderer.color = finishColor;
        FindObjectOfType<LevelManager>().ActualFinishNumber += 1;
        FindObjectOfType<LevelManager>().checkFinishLevel();

    }
  
    public void ResetBox()
    {
        canMove = true;
        renderer = GetComponent<SpriteRenderer>();
        renderer.color = initialColor;

    }
    // Update is called once per frame
    void Update()
    {
        checkFinishState();
      
    }

    void checkFinishState()
    {
        Vector3Int boxPosition = groundTilemap.WorldToCell(gameObject.transform.position);
        Vector3Int[] finishPosition = new Vector3Int[FindObjectOfType<LevelManager>().Finish.Length];
        for (int i = 0; i < FindObjectOfType<LevelManager>().Finish.Length; i++)
        {
            finishPosition[i] = groundTilemap.WorldToCell(FindObjectOfType<LevelManager>().Finish[i].position);
            if (finishPosition[i] == boxPosition && canMove == true)
            {
                FinishState();

            }
        }
       
    }
}
