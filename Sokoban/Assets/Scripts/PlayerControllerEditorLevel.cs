using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class PlayerControllerEditorLevel : MonoBehaviour
{
    [SerializeField]
    private Tilemap groundTilemap;
    [SerializeField]
    private Tilemap collisionTilemap;

    public Transform box;
    public Transform finish;
    private PlayerMovement controls;
    private AudioSource audio;
    // Start is called before the first frame update

    private void Awake()
    {
        
        controls = new PlayerMovement();
        box = FindObjectOfType<EditorLevelManager>().Box[0];
        finish = FindObjectOfType<EditorLevelManager>().Finish[0];

    }
    public void Start()
    {
      
        controls.Main.Movement.performed += ctx => Move(ctx.ReadValue<Vector2>());
        groundTilemap = GameObject.Find("Ground").GetComponent<Tilemap>();
        collisionTilemap = GameObject.Find("Col").GetComponent<Tilemap>();
    }
    private void OnEnable()
    {
        controls.Enable();
    }

    private void OnDisable()
    {
        controls.Disable();
    }


    private void Move(Vector2 direction)
    {
       if (CanMove(direction) && FindObjectOfType<EditorLevelManager>().finishState == false)
        {
            audio = GameObject.Find("MovementSound").GetComponent<AudioSource>();
            audio.Play();
            transform.position += (Vector3)direction;
            transform.position += ((Vector3)direction / 3.75f);
        }
    }
    private bool BoxPosition(Vector3Int gridPosition, Vector3Int[] boxPosition)
    {

        for (int i = 0; i < boxPosition.Length; i++)
        {
            if (boxPosition[i] != null)
            {
                if (gridPosition == boxPosition[i])
                {

                    box = FindObjectOfType<EditorLevelManager>().Box[i];
                    return true;

                }
            }
        }
        return false;
    }
    private bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = groundTilemap.WorldToCell(transform.position + (Vector3)direction);
        Vector3Int boxPosition = groundTilemap.WorldToCell(box.position);
        Vector3Int[] boxPosition2 = new Vector3Int[FindObjectOfType<EditorLevelManager>().Box.Length];
        for (int i = 0; i < FindObjectOfType<EditorLevelManager>().Box.Length; i++)
        {
            if (i == 0 && GameObject.Find("Box1") != null)
            {
                boxPosition2[i] = groundTilemap.WorldToCell(FindObjectOfType<EditorLevelManager>().Box[i].position);
            }if (i == 1 && GameObject.Find("Box2") != null)
            {
                boxPosition2[i] = groundTilemap.WorldToCell(FindObjectOfType<EditorLevelManager>().Box[i].position);
            }if (i == 2 && GameObject.Find("Box3") != null)
            {
                boxPosition2[i] = groundTilemap.WorldToCell(FindObjectOfType<EditorLevelManager>().Box[i].position);
            }if (i == 3 && GameObject.Find("Box4") != null)
            {
                boxPosition2[i] = groundTilemap.WorldToCell(FindObjectOfType<EditorLevelManager>().Box[i].position);
            }
        }

        Vector3Int finishPosition = groundTilemap.WorldToCell(finish.position);
        Vector3Int boxGridPosition = groundTilemap.WorldToCell(box.position + (Vector3)direction);

        if (!groundTilemap.HasTile(gridPosition) || collisionTilemap.HasTile(gridPosition))
        {
            Debug.Log("No");
            return false;
        }
        else if (BoxPosition(gridPosition, boxPosition2) == true && (!groundTilemap.HasTile(boxGridPosition) || collisionTilemap.HasTile(boxGridPosition)))
        {
            return false;
        }
        else if (FindObjectOfType<EditorLevelManager>().finishState == false && BoxPosition(gridPosition, boxPosition2) == true && groundTilemap.HasTile(boxGridPosition) && !collisionTilemap.HasTile(boxGridPosition) && box.GetComponent<BoxMevementLevelEditor>().canMove == true)
        {
            Debug.Log("Found");
            box.position += (Vector3)direction;
            box.position += ((Vector3)direction / 3.75f);
            return true;
        }
        else if (BoxPosition(gridPosition, boxPosition2) == true && box.GetComponent<BoxMevementLevelEditor>().canMove == false)
        {

            return false;
        }
        else
        {
           
            return true;
        }

        // Check finish state of the box 


    }
    // Update is called once per frame
    void Update()
    {

    }
    
}
