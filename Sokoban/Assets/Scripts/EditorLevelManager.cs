using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditorLevelManager : MonoBehaviour
{
    public bool EditorMode;

    public Transform InitialP;
    public Transform Player;
    public Transform[] Box;
    public Transform[] Finish;
    public Transform[] BoxInitialP;

    public bool finishState;
    public int ActualFinishNumber;

    public int finishNumber;
    public int boxesNumber;
    // Start is called before the first frame update
    void Start()
    {
        InitialP = Player;
        finishState = false;
        if (GameObject.Find("EditorManager") != null)
        {
            Player.GetComponent<PlayerControllerEditorLevel>().enabled = false;
        }
        else
        {
            Player.GetComponent<PlayerControllerEditorLevel>().enabled = true;

        }
    }
    public void SetPositions()
    {
        Player.position = InitialP.position;
        SetBoxes();
    }
    public void AddBox(Transform box, int boxId)
    {
        Box[boxId - 1] = box;
    } 
    public void AddFinish(Transform finish, int finishId)
    {
        Finish[finishId - 1] = finish;
    }
    public void SetBoxes()
    {
        for (int i = 0; i < Box.Length; i++)
        {
            Box[i].position = BoxInitialP[i].position;
            Box[i].gameObject.GetComponent<boxMovement>().ResetBox();
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
