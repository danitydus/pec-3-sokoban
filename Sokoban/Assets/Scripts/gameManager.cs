using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameManager : MonoBehaviour
{
    public GameObject[] levels;
    public GameObject actualLevel;
    public GameObject EndGamePanel;
    // Start is called before the first frame update
    void Start()
    {
        SetLevel();
    }
    public void SetLevel()
    {

        actualLevel = levels[PlayerPrefs.GetInt("LevelId") - 1];
        actualLevel.SetActive(true);
        CloseLevels();
    }
    public void CloseLevels()
    {
        for (int i = 0; i < levels.Length; i++)
        {
            if (i != PlayerPrefs.GetInt("LevelId") - 1)
            {
                levels[i].SetActive(false);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReturnMenu()
    {
        SceneManager.LoadScene(0);
    }
}
