using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditorDisplay : MonoBehaviour
{
    public SaveLevel levelEditor;
    public GameObject level;
    // Start is called before the first frame update
    void Start()
    {
        level = levelEditor.level;
        GameObject levelSpawn;
        levelSpawn = Instantiate(level, level.transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
