using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuManager : MonoBehaviour
{
    public GameObject play;
    public GameObject menu;
    // Start is called before the first frame update
    void Start()
    {
        play.SetActive(false);
        menu.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void LoadLevel(int levelId)
    {
        PlayerPrefs.SetInt("LevelId", levelId);
        SceneManager.LoadScene(1);
    }  public void LoadEditorLevel()
    {
        SceneManager.LoadScene(3);
    } 
    public void LoadEditor()
    {
        SceneManager.LoadScene(2);
    }

    public void loadLevelEditor()
    {
        PlayerPrefs.SetInt("LevelId", 0);
        SceneManager.LoadScene(3);

    }
}
