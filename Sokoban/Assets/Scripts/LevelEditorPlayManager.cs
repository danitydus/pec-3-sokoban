using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEditorPlayManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ReturnMenu()
    {
        SceneManager.LoadScene(0);
    } 
    public void endLevel()
    {
        StartCoroutine(LoadEndLevelRoutine());

    }

    private IEnumerator LoadEndLevelRoutine()
    {
        yield return new WaitForSecondsRealtime(3);
        ReturnMenu();
    }
}
