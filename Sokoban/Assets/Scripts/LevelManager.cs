using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LevelManager : MonoBehaviour
{
    public Transform InitialP;
    public Transform Player;
    public Transform[] Box;
    public Transform[] BoxInitialP;

    public Transform[] Finish;

    public TextMeshProUGUI levelIdText;
    public TextMeshProUGUI playerMovText;
    public TextMeshProUGUI boxMovText;

    public int playerMovements;
    public int boxMovements;

    public int ActualFinishNumber;
    public GameObject levelFinishedPanel;

    public bool finishState;
    // Start is called before the first frame update
    void Start()
    {
        finishState = false;
        levelFinishedPanel.SetActive(false);
        //SetUI();
        SetPositions();
    }
   

    public void SetPositions()
    {
        ActualFinishNumber = 0;
        playerMovements = 0;
        boxMovements = 0;
        Player.position = InitialP.position;
        SetBoxes();
    }
    public void SetBoxes()
    {
        for (int i = 0; i < Box.Length; i++)
        {
            Box[i].position = BoxInitialP[i].position;
            Box[i].gameObject.GetComponent<boxMovement>().ResetBox();
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetInt("LevelId") == 0)
        {
            levelIdText.SetText("Extra");
        }
        else
        {
            levelIdText.SetText("" + PlayerPrefs.GetInt("LevelId"));
        }
        playerMovText.SetText("" + playerMovements);
        boxMovText.SetText("" + boxMovements);

    }
    public void checkFinishLevel()
    {
        if (Finish.Length == ActualFinishNumber)
        {
            PlayerPrefs.SetInt("LevelId", PlayerPrefs.GetInt("LevelId") + 1);
            if (PlayerPrefs.GetInt("LevelId") == 0)
            {
                levelFinishedPanel.SetActive(true);
                FindObjectOfType<LevelEditorPlayManager>().endLevel();

            }
            else 
            {


                StartCoroutine(LoadNextLevelRoutine());
            }
        }
    }
    private IEnumerator LoadNextLevelRoutine()
    {
        finishState = true;
        levelFinishedPanel.SetActive(true);
        Time.timeScale = 0.0f;

        yield return new WaitForSecondsRealtime(3);
        if (PlayerPrefs.GetInt("LevelId") <= 10)
        {
            finishState = false;
            levelFinishedPanel.SetActive(false);

            FindObjectOfType<gameManager>().SetLevel();
        }

    }
}
