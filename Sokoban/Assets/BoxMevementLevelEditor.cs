using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BoxMevementLevelEditor : MonoBehaviour
{
    public bool canMove;
    public SpriteRenderer renderer;
    public Color finishColor;
    public Color initialColor;
    [SerializeField]
    private Tilemap groundTilemap;
    public Transform finish;
    private AudioSource audio;

    // Start is called before the first frame update
    void Start()
    {
        canMove = true;
        renderer = GetComponent<SpriteRenderer>();
    }
    public void FinishState()
    {
        canMove = false;
        renderer.color = finishColor;
        

    }

    public void ResetBox()
    {
        canMove = true;
        renderer = GetComponent<SpriteRenderer>();
        renderer.color = initialColor;

    }
    // Update is called once per frame
    void Update()
    {

    }

    void checkFinishState()
    {
       

    }
}
